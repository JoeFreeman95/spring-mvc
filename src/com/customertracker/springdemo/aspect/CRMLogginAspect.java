package com.customertracker.springdemo.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;



@Aspect
@Component
public class CRMLogginAspect {
	
	//setup logger
	
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	//Setup pointcut declaration
	@Pointcut("execution(* com.customertracker.springdemo.controller.*.*(..))")
	private void forControllerPackage() {}
	
	@Pointcut("execution(* com.customertracker.springdemo.service.*.*(..))")
	private void forServicePackage() {}
	
	
	@Pointcut("execution(* com.customertracker.springdemo.dao.*.*())")
	private void forDAOPackage() {}
	
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDAOPackage()")
	private void forAppFlow() {}
	
	//Add @before Advice
	@Before("forAppFlow()")
	public void before(JoinPoint theJoinPoint) {
		//display method being called
		String theMethod = theJoinPoint.getSignature().toShortString();
		myLogger.info("==>> In @Before: Calling Method:  " + theMethod);
		
		//display arguments to the method
		
		//get the Arguments
		
		Object[] args = theJoinPoint.getArgs();
		
		//loop through and display args
		for(Object tempArg : args) {
			myLogger.info("==>> argument:  "+ tempArg);
		}
	}
	

	//Add @afterReturning advice
	@AfterReturning(pointcut="forAppFlow()",
			returning="theResult")
	public void afterReturning(JoinPoint theJoinPoint, Object theResult) {
		
		//Display method returning from
		String theMethod = theJoinPoint.getSignature().toShortString();
		myLogger.info("==>> In @AfterReturning: From  Method:  " + theMethod);
		
		//Display Data returned
		myLogger.info("===>> result:  "+ theResult);

	}
	
	
	
	
	
	
	
	
	

}
